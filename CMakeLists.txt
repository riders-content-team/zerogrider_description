cmake_minimum_required(VERSION 3.0.2)
project(zerogrider_description)

add_definitions(-std=c++11)


find_package(catkin REQUIRED COMPONENTS
  rospy
  roscpp
  std_msgs
  geometry_msgs
  gazebo_ros
  message_generation
)

add_service_files(
  FILES
  CameraService.srv
  LaserService.srv
)

generate_messages(
  DEPENDENCIES std_msgs geometry_msgs
)

catkin_package(
  CATKIN_DEPENDS
    message_runtime
    gazebo_ros
    std_msgs
    roscpp
  DEPENDS
    gazebo
    SDF
    roscpp
    gazebo_ros
    message_runtime
    std_msgs
)

find_package(gazebo REQUIRED)

link_directories(${GAZEBO_LIBRARY_DIRS})

include_directories(${Boost_INCLUDE_DIR} ${catkin_INCLUDE_DIRS} ${GAZEBO_INCLUDE_DIRS})
include_directories(${roscpp_INCLUDE_DIRS})
include_directories(${std_msgs_INCLUDE_DIRS} ${SDFormat_INCLUDE_DIRS})

add_library(ForcePlugin plugins/ForcePlugin.cc)
add_library(contact SHARED plugins/ContactPlugin.cc)
add_library(CameraSensorPlugin plugins/CameraSensorPlugin.cc)
add_library(LaserSensorPlugin plugins/LaserSensorPlugin.cc)

add_dependencies(ForcePlugin ${catkin_EXPORTED_TARGETS})
add_dependencies(contact ${catkin_EXPORTED_TARGETS})
add_dependencies(CameraSensorPlugin ${catkin_EXPORTED_TARGETS} zerogrider_description_generate_messages_cpp)
add_dependencies(LaserSensorPlugin ${catkin_EXPORTED_TARGETS} zerogrider_description_generate_messages_cpp)

target_link_libraries(ForcePlugin ${roscpp_LIBRARIES} ${GAZEBO_LIBRARIES})
target_link_libraries(contact ${roscpp_LIBRARIES} ${GAZEBO_LIBRARIES})
target_link_libraries(CameraSensorPlugin ${catkin_LIBRARIES} ${GAZEBO_LIBRARIES} ${Boost_LIBRARIES})
target_link_libraries(LaserSensorPlugin ${catkin_LIBRARIES} ${GAZEBO_LIBRARIES} ${Boost_LIBRARIES})











