#include "CameraSensorPlugin.hh"

using namespace gazebo;
// Register this plugin with the simulator
GZ_REGISTER_SENSOR_PLUGIN(CameraSensorPlugin)

// Constructor
CameraSensorPlugin::CameraSensorPlugin() : SensorPlugin()
{
}

// Destructor
CameraSensorPlugin::~CameraSensorPlugin()
{
}

void CameraSensorPlugin::Load(sensors::SensorPtr _sensor, sdf::ElementPtr _sdf)
{
  this->sensor = _sensor;
  this->world = physics::get_world(this->sensor->WorldName());

  // Check sensor
  if (!_sensor)
    gzerr << "Invalid sensor pointer.\n";

  // Get sensor
	this->parentSensor =
	std::dynamic_pointer_cast<sensors::CameraSensor>(_sensor);
	this->camera = this->parentSensor->Camera();

  // Check parent sensor
	if (!this->parentSensor)
	{
    gzerr << "CameraPlugin not attached to a camera sensor\n";
    return;
	}

  // Get robot namespace
  if (!_sdf->HasElement("robotNamespace"))
  {
    this->robot_namespace_ = "robot";
  } else {
    this->robot_namespace_ = _sdf->Get<std::string>("robotNamespace");
  }

  // Get camera name
  if (!_sdf->HasElement("cameraName"))
  {
    this->camera_name = "camera";
  } else {
    this->camera_name = _sdf->Get<std::string>("cameraName");
  }

  // Get frame name
  if (!_sdf->HasElement("frameName"))
  {
    this->frame_name_ = "world";
  } else {
    this->frame_name_ = _sdf->Get<std::string>("frameName");
  }

  // Get topic name
  // If empty generate from frame name
  if (!_sdf->HasElement("imageTopicName"))
  {
    this->topic_name.append(frame_name_);
    this->topic_name.append("/image");
  } else {
    this->topic_name = this->camera_name + "/" + _sdf->Get<std::string>("imageTopicName");
    this->scaled_topic_name = this->topic_name + "_scaled";
  }

  // Get service name
  // If empty generate from frame name
  if (!_sdf->HasElement("imageServiceName"))
  {
    this->service_name.append(frame_name_);
    this->service_name.append("/get_image");

  } else {
    this->service_name = this->camera_name + "/" + _sdf->Get<std::string>("imageServiceName");
  }

  // Get camera parameters
	this->width = this->camera->ImageWidth();
	this->height = this->camera->ImageHeight();
  this->format = this->camera->ImageFormat();
  this->scale_rate = 10;
  this->image = (char *)malloc(this->width * this->height * 3);
  this->topic_image = (char *)malloc(this->width * this->height * 3 * pow(this->scale_rate, 2));
  this->sequence = 0;

  // Initialize ros, if it has not already bee initialized.
  if (!ros::isInitialized())
  {
    int argc = 0;
    char **argv = NULL;
    ros::init(argc, argv, robot_namespace_,
      ros::init_options::NoSigintHandler);
  }

  // Create our ROS node. This acts in a similar manner to
  // the Gazebo node
  this->rosNode.reset(new ros::NodeHandle(robot_namespace_));

  // Image Publisher
  //this->color_sensor_scaled_publisher = this->rosNode->advertise<sensor_msgs::Image>(this->scaled_topic_name, 1000);
  this->color_sensor_publisher = this->rosNode->advertise<sensor_msgs::Image>(this->topic_name, 1000);

  // Service Options
  ros::AdvertiseServiceOptions color_sensor_options =
  ros::AdvertiseServiceOptions::create<zerogrider_description::CameraService>(
    this->service_name,
      boost::bind(&CameraSensorPlugin::ColorCallback, this, _1, _2),
        ros::VoidConstPtr(), NULL);

  // Advertise Service
  this->color_service = this->rosNode->advertiseService(color_sensor_options);

  // Init image Callback
	this->newFrameConnection = this->camera->ConnectNewImageFrame(
	  std::bind(&CameraSensorPlugin::OnNewFrame, this,
	    std::placeholders::_1, std::placeholders::_2, std::placeholders::_3,
        std::placeholders::_4, std::placeholders::_5));

  // This must be called
	this->parentSensor->SetActive(true);
}

// Image Callback
void CameraSensorPlugin::OnNewFrame(const unsigned char *_image,
  unsigned int _width, unsigned int _height, unsigned int _depth,
    const std::string &_format)
{
  // Get sim time
  double sim_time = this->world->SimTime().Double();

  // Copy image to class objects
  this->height = _height;
  this->width = _width;
  uint32_t image_size = 3 * this->height * this->width;
  this->image = (char*) realloc(this->image, image_size);
  memcpy(this->image, _image, image_size);

  // Scale image for better view at camera
  uint32_t topic_image_size = image_size * pow(this->scale_rate, 2);

  //CameraSensorPlugin::ScaleImage(this->image, this->height, this->width, this->scale_rate);

  // publish scaled message
  // Copy data into ros msg
  /*
  this->scaled_image_msg_.header.stamp = ros::Time(sim_time);
  this->scaled_image_msg_.header.frame_id = this->frame_name_;

  this->scaled_image_msg_.height = this->height * this->scale_rate;
  this->scaled_image_msg_.width = this->width * this->scale_rate;
  this->scaled_image_msg_.encoding = "rgb8";
  this->scaled_image_msg_.is_bigendian = 0;
  this->scaled_image_msg_.step = this->width * 3 * this->scale_rate;
  this->scaled_image_msg_.data.resize(topic_image_size);
  memcpy(&this->scaled_image_msg_.data[0], this->topic_image, topic_image_size);

  // Publish msg
  this->color_sensor_scaled_publisher.publish(this->scaled_image_msg_);
*/
  // publish image message without scaling
  this->image_msg_.header.stamp = ros::Time(sim_time);
  this->image_msg_.header.frame_id = this->frame_name_;

  this->image_msg_.height = this->height;
  this->image_msg_.width = this->width;
  this->image_msg_.encoding = "rgb8";
  this->image_msg_.is_bigendian = 0;
  this->image_msg_.step = this->width * 3;
  this->image_msg_.data.resize(image_size);
  memcpy(&this->image_msg_.data[0], this->image, image_size);

  // Publish msg
  this->color_sensor_publisher.publish(this->image_msg_);

  // Handle ROS
  ros::spinOnce();
}

bool CameraSensorPlugin::ColorCallback(
  zerogrider_description::CameraService::Request &req,
    zerogrider_description::CameraService::Response &res)
{
  uint32_t image_size = 3 * this->height * this->width;
  double sim_time = this->world->SimTime().Double();

  // Header
  res.seq = this->sequence;
  this->sequence++;
  res.stamp = ros::Time(sim_time);
  res.frame_id = this->frame_name_;

  // Image data
	res.height = this->height;
	res.width = this->width;
  res.encoding = this->format;
	res.is_bigendian = 0;
  res.step = image_size;
  res.data.resize(image_size);
  memcpy(&res.data[0], this->image, image_size);

  return true;
}

// Scales image in given rate
void CameraSensorPlugin::ScaleImage(const char* image, int height, int width, uint8_t scale_rate) {

  // Don't ask how i come up with this algorithm
  uint16_t image_size = height * width * 3;
  uint16_t scaled_image_size = image_size * pow(scale_rate, 2);
  this->topic_image = (char*)realloc(this->topic_image, scaled_image_size);

  // Height loop
  for (uint16_t i = 0 ; i < height; i++) {
    // Scale loop
    for (uint16_t m  = 0; m < scale_rate; m++) {
      // Width loop
      for (uint16_t j  = 0; j < width; j++) {
        // Scale loop
        for (uint16_t k  = 0; k < scale_rate; k++) {
          // Adjusts horizontal axis
          memcpy(this->topic_image + (i * width * 3) + (scale_rate * j * 3) + (k * 3), image + (i * width * 3) + (j * 3), 3);
        }
      }
      // Adjusts vertical axis
      memcpy(this->topic_image + (m * width * scale_rate * 3), this->topic_image + (i * width * scale_rate * 3), width * scale_rate * 3);
    }
  }
}
